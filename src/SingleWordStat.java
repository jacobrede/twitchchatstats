
public class SingleWordStat implements Comparable{
		private String w;
		private int o;
		
		SingleWordStat(String word, int occurrance) {
			this.w = word;
			this.o = occurrance;
		}
		public void incrementOccurrance() {
			this.o++;
		}
		public int getWordOccurrance() {
			return this.o;
		}
		@Override
		public int compareTo(Object o) {
			if(o instanceof SingleWordStat) {
				SingleWordStat copyOfWord = (SingleWordStat) o;
				if(this.o > copyOfWord.getWordOccurrance()) {
					return 1;
				} else if(this.o < copyOfWord.getWordOccurrance()){
					return -1;
				} else {
					return 0;
				}
			} else {
				return -1;
			}
		}
	}