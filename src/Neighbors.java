import java.util.ArrayList;

import org.joda.time.DateTime;

public class Neighbors {
	private ArrayList<DateTime> neighborstart_time;
	private DateTime start_time;
	Neighbors(DateTime start_time) {
		this.start_time = new DateTime(start_time);
		neighborstart_time = new ArrayList<DateTime>();
	}
	public DateTime getStart_time() {
		return start_time;
	}
	public ArrayList<DateTime> getNeighborstart_time() {
		return neighborstart_time;
	}
}
