import java.io.IOException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.bson.Document;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.mysql.jdbc.Connection;

public class vodMain {
	static ArrayList<String> channelsToJoin = new ArrayList<String>();
	static Connection conn = null;
	
	private final static String DB_URL = "jdbc:mysql://localhost:3306/bot?characterEncoding=UTF-8&useUnicode=true&useSSL=false";
	private final static String DB_USER = "root";
	private final static String DB_PASS = "REMOVED";
	private final static DateTimeFormatter df2 = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
	private final static DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
	private static MongoDatabase mongoDB;
	
	public static void main(String[] args) {
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
            conn = (Connection) DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
            // Do something with the Connection
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    	MongoClientURI uri = new MongoClientURI("mongodb://vodsumAdmin:REMOVED@localhost:27017/?authSource=vodsum");
    	//MongoClientURI uri = new MongoClientURI("mongodb://vodsumAdmin:REMOVED@104.131.229.28:27017/?authSource=vodsum");
    	MongoClient mongoClient = new MongoClient(uri);
    	mongoDB = mongoClient.getDatabase("vodsum");
    	
		loadChannels();
		
		for(String usernameToCheckStream : channelsToJoin) {
        	//get all vod info for every channel in the list
			
			try {
				for(JsonElement singleVod : getUserVodData(usernameToCheckStream)) {
					//System.out.println(singleVod);
					
					DateTime recorded_at = new DateTime(df2.parseDateTime(singleVod.getAsJsonObject().get("recorded_at").getAsString()));
					//set timezone difference here
					//System.out.println(recorded_at);
					DateTime ended_at = new DateTime(recorded_at);
					ended_at = ended_at.plusSeconds(singleVod.getAsJsonObject().get("length").getAsInt());
					String _id = singleVod.getAsJsonObject().get("_id").getAsString();
					String title = singleVod.getAsJsonObject().get("title").getAsString();
					String URL = singleVod.getAsJsonObject().get("url").getAsString();
					String status = singleVod.getAsJsonObject().get("status").getAsString();
					if(!vodExists(_id)) {
						//insert vod info
						if(status.equals("recorded")) {
							//make sure the vod is done recording
							try {
								insertNewVod(recorded_at, ended_at, _id, title, URL, usernameToCheckStream);
								setStats(recorded_at, ended_at, usernameToCheckStream);
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		//do delete of 2 day old messages
		deleteOldMessages();
	}
	private static void deleteOldMessages() {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			Timestamp current_time = new Timestamp(new DateTime( DateTimeZone.UTC ).minusDays(2).toDate().getTime());
			stmt.executeUpdate("DELETE FROM Messages WHERE created_at < '"+current_time+"';");
		} catch (SQLException e) {
		} finally {
	        if (stmt != null) { 
	        	try {
	        		stmt.close();
	        	} catch (SQLException e) {} 
	        }
	    }
	}
	private static void insertNewVod(DateTime recorded_at, DateTime ended_at, String _id, String title, String URL, String username) throws ParseException {
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement("INSERT INTO Streams (username, start_time, end_time, URL, title, _id) VALUES (?, ?, ?, ?, ?, ?)");
			stmt.setString(1, username);
			stmt.setTimestamp(2, new Timestamp(recorded_at.toDate().getTime()));
			stmt.setTimestamp(3, new Timestamp(ended_at.toDate().getTime()));
			stmt.setString(4, URL);
			stmt.setString(5, title);
			stmt.setString(6, _id);
			stmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
	        if (stmt != null) { 
	        	try {
	        		stmt.close();
	        	} catch (SQLException e) {} 
	        }
	    }
		mongoDB.getCollection("Streams").insertOne(new Document("username", username)
				.append("start_time", format.format(recorded_at.toDate()))
				.append("end_time", format.format(ended_at.toDate()))
				.append("URL", URL)
				.append("title", title)
				.append("code", _id)
				.append("created_at",format.format(new Date()))
				.append("deleted_at", 0)
			);
		
	}
	private static JsonArray getUserVodData(String username) throws IOException {
		//https://api.twitch.tv/kraken/channels/lirik/videos?broadcasts=true&client_id=mpd185lnzowr67ptsfljm2xsdg15pxx
		String channerUrl = "https://api.twitch.tv/kraken/channels/"+ username +"/videos?broadcasts=true&client_id=mpd185lnzowr67ptsfljm2xsdg15pxx";

        String jsonText = readFromUrl(channerUrl);// reads text from URL

        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(jsonText).getAsJsonObject();
        JsonArray jsonStream = json.get("videos").getAsJsonArray();
        return jsonStream;
        //return jsonStream.get("created_at").getAsString();
	}

    private static String readFromUrl(String url) throws IOException {
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpGet request = new HttpGet(url);
            request.addHeader("content-type", "application/json");
            HttpResponse result = httpClient.execute(request);
            String json = EntityUtils.toString(result.getEntity(), "UTF-8");
            return json;
        }
    }
    private static boolean vodExists(String _id) {
    	Statement stmt = null;
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT COUNT(*) AS found FROM Streams WHERE _id='"+_id+"';");
			while (rs.next()) {
				int vodExists = rs.getInt("found");
				if(vodExists >= 1) {
					return true;
				}
				
			}
		} catch (SQLException e) {
			
		} finally {
	        if (stmt != null) { 
	        	try {
	        		stmt.close();
	        	} catch (SQLException e) {} 
	        }
	    }
		return false;
    }
    private static void loadChannels() {
    	//first get usersToRecord from mongodb
		FindIterable<Document> iterable = mongoDB.getCollection("UsersToRecord").find();
		iterable.forEach(new Block<Document>() {
		    @Override
		    public void apply(final Document document) {
		    	String usernameToRecordChat = (String) document.get("username");
		    	if(!channelsToJoin.contains(usernameToRecordChat)) {
					channelsToJoin.add(usernameToRecordChat);
				}
		    }
		});
		//Now check if SQL is updated
		Statement stmt = null;
    	ArrayList<String> SQLChannelsToJoin = new ArrayList<String>();
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM UsersToRecord;");
			while (rs.next()) {
				String usernameToRecordChat = rs.getString("username");
				if(!SQLChannelsToJoin.contains(usernameToRecordChat)) {
					//load SQL users that were recording from the SQL db
					SQLChannelsToJoin.add(usernameToRecordChat);
				}
			}
			for(String username : channelsToJoin) {
				if(!SQLChannelsToJoin.contains(username)) {
					//first make sure the user exists
					if(userExists(username)) {
						// add new user to SQL db since SQL doesnt contain the user from mongodb
						PreparedStatement pstmt = null;
						pstmt = conn.prepareStatement("INSERT INTO UsersToRecord (username) VALUES (?)");
						pstmt.setString(1, username);
						pstmt.executeUpdate();
					} else {
						//need to insert before
						insertNewUser(username);
						//now add the user to record
						PreparedStatement pstmt = null;
						pstmt = conn.prepareStatement("INSERT INTO UsersToRecord (username) VALUES (?)");
						pstmt.setString(1, username);
						pstmt.executeUpdate();
					}
					
					
					
				}
			}

			//now loop through the mongodbusers
//			for(String username : channelsToJoin) {
//				if(!mongoDBusernames.contains(username)) {
//					//add to mongodb
//					mongoDB.getCollection("UsersToRecord").insertOne(new Document("username",username)
//							.append("created_at", format.format(new Date()))
//							.append("deleted_at", 0));
//				}
//			}
		} catch (SQLException e) {
			
		} finally {
	        if (stmt != null) { 
	        	try {
	        		stmt.close();
	        	} catch (SQLException e) {} 
	        }
	    }
    }
    private static void insertNewUser(String username) {
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement("INSERT INTO TwitchUsers (username) VALUES (?)");
			stmt.setString(1, username);
			stmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
	        if (stmt != null) { 
	        	try {
	        		stmt.close();
	        	} catch (SQLException e) {} 
	        }
	    }
	}
    private static boolean userExists(String username) {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT COUNT(*) AS found FROM TwitchUsers WHERE username='"+username+"';");
			while (rs.next()) {
				if(rs.getInt("found") != 0) {
					return true;
				}
			}
		} catch (SQLException e) {
			return false;
		} finally {
	        if (stmt != null) { 
	        	try {
	        		stmt.close();
	        	} catch (SQLException e) {} 
	        }
	    }
		return false;
	}
    private static void setStats(DateTime recorded_at, DateTime ended_at, String username) {
    	//start & endtime loaded, go min, by min
    	DateTime tempStartTime = new DateTime(recorded_at);
		//need to batch insert these modifications
		ArrayList<String> SQLToExecuted = new ArrayList<String>();
		ArrayList<Stats> allStats = new ArrayList<Stats>();
		ArrayList<Document> mongoDbMassInsert = new ArrayList<Document>();
		//set stats on a per min basis
		while(tempStartTime.isBefore(ended_at)) {
			DateTime tempEndTime = new DateTime(tempStartTime.plusMinutes(1));
			Stats newMinStat = new Stats(username, tempStartTime.toDate(), tempEndTime.toDate(), new Timestamp(recorded_at.toDate().getTime()), DB_URL, DB_USER, DB_PASS);
			allStats.add(newMinStat);
			SQLToExecuted.add(newMinStat.getSQL());
			mongoDbMassInsert.add(newMinStat.getMongoDocumentToInsert());
			tempStartTime = tempEndTime;
		}
		
		Collections.sort(allStats,Collections.reverseOrder());
		int top10 = (int) (allStats.size() / 10);//get top posts only amount
		ArrayList<Integer> locationsThatAreNeighbors = new ArrayList<Integer>();
		
		for(int j = 0; j < top10; j++) {
			DateTime currentStartTimePlus1 = new DateTime(allStats.get(j).getStart_time().getTime());
			currentStartTimePlus1 = currentStartTimePlus1.plusMinutes(1);
			DateTime currentStartTimeMinus1 = new DateTime(allStats.get(j).getStart_time().getTime());
			currentStartTimeMinus1 = currentStartTimeMinus1.minusMinutes(1);
			//System.out.println("inside top20");
			for(int k = 0; k < top10; k++) {
				if(k != j) {
					if(!locationsThatAreNeighbors.contains(j)) {
						if(currentStartTimePlus1.equals(new DateTime(allStats.get(k).getStart_time()))) {
							locationsThatAreNeighbors.add(j);
							//System.out.println("add neighbor1");
						} else if(currentStartTimeMinus1.equals(new DateTime(allStats.get(k).getStart_time()))) {
							locationsThatAreNeighbors.add(j);
							//System.out.println("add neighbor2");
						}
					}
				}
			}
		}
		
		ArrayList<DateTime> neighborStartTimesOrganized = new ArrayList<DateTime>();
		for(Integer hasNeighborPosition : locationsThatAreNeighbors) {
			neighborStartTimesOrganized.add(new DateTime(allStats.get(hasNeighborPosition).getStart_time()));
		}
		//actually sort the start times
		Collections.sort(neighborStartTimesOrganized);
		//System.out.println("sorted starttime");
		ArrayList<Top10Stat> allTop10Stats = new ArrayList<Top10Stat>();
		DateTime lastNeighborStart_time = new DateTime();
		
		for(DateTime currentStart_time : neighborStartTimesOrganized) {
			DateTime currentEnd_time = new DateTime(getEndFromStartStat(currentStart_time, allStats));
			//System.out.println(currentStart_time);
			DateTime tempLastMin = new DateTime(currentStart_time.minusMinutes(1));
			if(lastNeighborStart_time.equals(tempLastMin)) {
				//neighbor confirmed, add stat to current top10stat
				if(findStart_timeStat(currentStart_time, allStats) != null) {
					allTop10Stats.get(allTop10Stats.size()-1).addStat(findStart_timeStat(currentStart_time, allStats));
					allTop10Stats.get(allTop10Stats.size()-1).setEnd_time(currentEnd_time);//update end time (overwrite)
					//System.out.println("modified top10");
				}
			} else {
				//not neighbor create new top10stat to be used
				allTop10Stats.add(new Top10Stat(username, currentStart_time, currentEnd_time, recorded_at));
				//System.out.println("add top10");
				//also if a previous top10stat exists you need to set its endtime to the last start_time+1(end_time)
			}
			lastNeighborStart_time = new DateTime(currentStart_time);
		}
		for(int j = 0; j < top10; j++) {
			if(!locationsThatAreNeighbors.contains(j)) {
				//not neighbor, but top 10
				allTop10Stats.add(new Top10Stat(username, new DateTime(allStats.get(j).getStart_time().getTime()), new DateTime(allStats.get(j).getEnd_time().getTime()), recorded_at));
				allTop10Stats.get(allTop10Stats.size()-1).addStat(allStats.get(j));
			}
		}
		ArrayList<Document> mongoDbMassInsertNeighbors = new ArrayList<Document>();
		
		//System.out.println("allTop10Stats length: "+allTop10Stats.size());
		for(int j =0; j < allTop10Stats.size(); j++) {
			//System.out.println("add insert top10");
			allTop10Stats.get(j).sortWords();//must be called before
//			System.out.println("before sql top10");
//			System.out.println(allTop10Stats.get(j).getSQLInsertCommand());
			SQLToExecuted.add(allTop10Stats.get(j).getSQLInsertCommand());
			//System.out.println("after sql top10");
			mongoDbMassInsertNeighbors.add(new Document(allTop10Stats.get(j).getMongoDocumentToInsert()));
			//System.out.println("after mongo top10");
		}
		
		Statement SQLstmt = null;
		try {
			SQLstmt = conn.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for(String SQL : SQLToExecuted) {
			try {
				SQLstmt.addBatch(SQL);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//System.out.println("beforemass execute");
		try {
			SQLstmt.executeBatch();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(mongoDbMassInsertNeighbors.size() > 0) {
			mongoDB.getCollection("StreamTop10Stats").insertMany(mongoDbMassInsertNeighbors);
		}
		if(mongoDbMassInsert.size() > 0) {
			mongoDB.getCollection("BestMinByMinStats").insertMany(mongoDbMassInsert);
		}
		
		//System.out.println("after mongodb execute");
		//set the stats created flag to true
		PreparedStatement prestmt = null;
		try {
			prestmt = conn.prepareStatement("UPDATE Streams SET stats_created = 1 WHERE username = ? AND start_time = ? AND stats_created IS NULL");
			prestmt.setString(1, username);
			prestmt.setTimestamp(2, new Timestamp(recorded_at.toDate().getTime()));
			prestmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
	        if (prestmt != null) { 
	        	try {
	        		prestmt.close();
	        	} catch (SQLException e) {} 
	        }
	    }
    }
    private static Stats findStart_timeStat(DateTime start_time, ArrayList<Stats> allStats) {
    	//System.out.println("inside findStart_timeStat");
    	for(Stats singleStat : allStats) {
    		if(new DateTime(singleStat.start_time).equals(start_time)) {
    			return singleStat;
    		}
    	}
    	return null;
    }
    private static DateTime getEndFromStartStat(DateTime start_time, ArrayList<Stats> allStats) {
    	for(Stats singleStat : allStats) {
    		if(new DateTime(singleStat.start_time).equals(start_time)) {
    			return new DateTime(singleStat.getEnd_time());
    		}
    	}
    	return null;
    }
}
