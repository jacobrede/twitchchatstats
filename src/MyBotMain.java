import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.bson.Document;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.mysql.jdbc.Connection;

public class MyBotMain {
    
	static ArrayList<String> channelsToJoin = new ArrayList<String>();
	static Connection conn = null;
	
	private final static String DB_URL = "jdbc:mysql://localhost:3306/bot?characterEncoding=UTF-8&useUnicode=true&useSSL=false";
	private final static String DB_USER = "root";
	private final static String DB_PASS = "REMOVED";
	private final static DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
	private static MongoDatabase mongoDB;
	
    public static void main(String[] args) throws Exception {
    	try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	try {
            conn = (Connection) DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
            // Do something with the Connection
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    	//remote
    	//MongoClientURI uri = new MongoClientURI("mongodb://vodsumAdmin:REMOVED@104.131.229.28:27017/?authSource=vodsum");
    	
    	//on server (prod)
    	MongoClientURI uri = new MongoClientURI("mongodb://vodsumAdmin:REMOVED@localhost:27017/?authSource=vodsum");
    	
    	//on local machine (dev)
    	//MongoClientURI uri = new MongoClientURI("mongodb://localhost:3001/?authSource=vodsum");
    	
    	MongoClient mongoClient = new MongoClient(uri);
    	mongoDB = mongoClient.getDatabase("vodsum");
    	loadChannels();
    	
        // Now start our bot up.
        MyBot bot = new MyBot(DB_URL, DB_USER, DB_PASS);
        
        // Enable debugging output.
        bot.setVerbose(false);
        // Connect to the IRC server.
        bot.connect("irc.twitch.tv", 6667, "REMOVED");

        // Join the #pircbot channel.
        for(String username : channelsToJoin) {
        	bot.joinChannel("#"+username);
        }
        
    }
    private static void loadChannels() {
    	//first get usersToRecord from mongodb, only go off mongodb for now
		FindIterable<Document> iterable = mongoDB.getCollection("UsersToRecord").find();
		iterable.forEach(new Block<Document>() {
		    @Override
		    public void apply(final Document document) {
		    	String usernameToRecordChat = (String) document.get("username");
		    	if(!channelsToJoin.contains(usernameToRecordChat)) {
					channelsToJoin.add(usernameToRecordChat);
				}
		    }
		});
    }
 }
