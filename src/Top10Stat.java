import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;

import org.bson.Document;
import org.joda.time.DateTime;

import com.google.gson.Gson;

public class Top10Stat {
	private String username;
	private DateTime start_time;//key
	private DateTime stream_start_time;
	private DateTime end_time;
	private int totalMessagesCount;
	private HashMap<String, Integer> wordOccurrance;
	private ArrayList<SingleWordStat> sortedWordOccurrence;
	Top10Stat(String input_username, DateTime statStart_time, DateTime statEnd_time, DateTime statStreamStart_time) {
		username = input_username;
		start_time = new DateTime(statStart_time);
		end_time = new DateTime(statEnd_time);
		stream_start_time = new DateTime(statStreamStart_time);
		wordOccurrance = new HashMap<String, Integer>();
		sortedWordOccurrence = new ArrayList<SingleWordStat>();
		totalMessagesCount = 0;
	}
	
	public DateTime getStart_time() {
		return start_time;
	}
	public DateTime getEnd_time() {
		return end_time;
	}
	public void setEnd_time(DateTime newEnd_time) {
		end_time = new DateTime(newEnd_time);
	}

	public int getTotalMessagesCount() {
		return totalMessagesCount;
	}
	
	public void addStat(Stats newStat) {
		for(String keyToAdd : newStat.getWordOccurranceMap().keySet()) {
			if(!wordOccurrance.containsKey(keyToAdd)) {
				wordOccurrance.put(keyToAdd, newStat.getWordOccurranceMap().get(keyToAdd));
			} else {
				int oldCount = wordOccurrance.get(keyToAdd);
				wordOccurrance.put(keyToAdd, (newStat.getWordOccurranceMap().get(keyToAdd)+oldCount));
			}
			
		}
		totalMessagesCount += newStat.getNumberMessages();
	}
	public DateTime getStream_start_time() {
		return stream_start_time;
	}
	public void sortWords() {
		for(String singleWord : wordOccurrance.keySet()) {
			SingleWordStat newWordStat = new SingleWordStat(singleWord, wordOccurrance.get(singleWord));
			sortedWordOccurrence.add(newWordStat);
		}
		//sort, desc
		Collections.sort(sortedWordOccurrence ,Collections.reverseOrder());
	}
	public String getSQLInsertCommand() {
		return "INSERT INTO StreamTop10Stats (username, start_time, end_time, messagesPerMin, TopWords, stream_start_time) VALUES ("
				+ "'"+username+"', "
				+ "'"+new Timestamp(start_time.toDate().getTime())+"', "
				+ "'"+new Timestamp(end_time.toDate().getTime())+"', "
				+ ""+totalMessagesCount+", "
				+ "'"+new Gson().toJson(sortedWordOccurrence)+"',"
				+ "'"+new Timestamp(this.stream_start_time.toDate().getTime())+"')";
	}
	public Document getMongoDocumentToInsert() {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
		return new Document("username", username)
				.append("start_time", format.format(start_time.toDate()))
				.append("end_time", format.format(end_time.toDate()))
				.append("stream_start_time", format.format(stream_start_time.toDate()))
				.append("messagesPerMin", totalMessagesCount)
				.append("TopWords", new Gson().toJson(sortedWordOccurrence));
	}
}
