import java.io.StringReader;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.jibble.pircbot.*;
import org.joda.time.DateTime;

import com.mysql.jdbc.Connection;

public class MyBot extends PircBot implements Runnable {
	
	private int TimeZoneOffSetMillis = 0;
	
	Connection conn = null;
	
	private ConcurrentLinkedQueue<ChatMessage> queue;
	private PreparedStatement mainStatement;
	
	ExecutorService consumer = new ThreadPoolExecutor(1, 2, 30, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(100));
	
//	private LinkedList<Thread> threads = new LinkedList<Thread>();
	
    public MyBot(String DB_URL, String DB_USER, String DB_PASS) {
        this.setName("metal_burning");
        this.queue = new ConcurrentLinkedQueue<ChatMessage>();
        
        //setup the DB driver
        try {
            conn = (Connection) DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
            // Do something with the Connection
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        
        //setup prepared statement
        try {
			this.mainStatement = conn.prepareStatement("INSERT INTO Messages (username, channel, message, created_at) VALUES (?, ?, ?, ?)");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        TimeZone tz = Calendar.getInstance().getTimeZone();
        this.TimeZoneOffSetMillis = tz.getOffset(new Date().getTime());
//        Consumer worker = new Consumer();
//        Consumer worker1 = new Consumer();
//        threads.add(new Thread(worker));
//        threads.add(new Thread(worker1));
//        threads.getFirst().start();
//        threads.get(1).start();
        ExecutorService executorService = Executors.newFixedThreadPool(4);

        executorService.execute(new Runnable() {
        	@Override
    		public void run() {
    			while(!Thread.currentThread().isInterrupted()) {
    				try {
    					//execute batch of statements
						mainStatement.executeBatch();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    			}
    		}
//    		private void insertNewMessage(PreparedStatement batchstmt, ChatMessage removedMessage) {
//    			PreparedStatement stmt = null;
//    			try {
//    				stmt = conn.prepareStatement("INSERT INTO Messages (username, channel, message, created_at) VALUES (?, ?, ?, ?)");
//    				stmt.setString(1, removedMessage.getSender());
//    				stmt.setString(2, removedMessage.getChannel());
//    				stmt.setCharacterStream(3, new StringReader(removedMessage.getMessage()), removedMessage.getMessage().length());
//    				stmt.setTimestamp(4, new Timestamp(new DateTime().minusMillis(TimeZoneOffSetMillis).toDate().getTime()));
//    				stmt.executeUpdate();
//    				stmt.addBatch();
//    			} catch (SQLException e) {
//    				// TODO Auto-generated catch block
//    				e.printStackTrace();
//    			} finally {
//    		        if (stmt != null) { 
//    		        	try {
//    		        		stmt.close();
//    		        	} catch (SQLException e) {} 
//    		        }
//    		    }
//    			//System.out.println("Inserted Message "+new Date().toString()+" From "+removedMessage.getSender()+" in "+removedMessage.getChannel()+": "+removedMessage.getMessage()+"");
//    		}
        });
        
        try {
            conn = (Connection) DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
            // Do something with the Connection
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }
    private boolean userExists(ChatMessage removedMessage) {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT COUNT(*) AS found FROM TwitchUsers WHERE username='"+removedMessage.getSender()+"';");
			while (rs.next()) {
				if(rs.getInt("found") != 0) {
					return true;
				}
			}
		} catch (SQLException e) {
			return false;
		} finally {
	        if (stmt != null) { 
	        	try {
	        		stmt.close();
	        	} catch (SQLException e) {} 
	        }
	    }
		return false;
	}
	private boolean channelExists(ChatMessage removedMessage) {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT COUNT(*) AS found FROM TwitchUsers WHERE username='"+removedMessage.getChannel()+"';");
			while (rs.next()) {
				if(rs.getInt("found") != 0) {
					return true;
				}
			}
		} catch (SQLException e) {
			return false;
		} finally {
	        if (stmt != null) { 
	        	try {
	        		stmt.close();
	        	} catch (SQLException e) {} 
	        }
	    }
		return false;
	}
	private void insertNewUser(ChatMessage removedMessage) {
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement("INSERT INTO TwitchUsers (username) VALUES (?)");
			stmt.setString(1, removedMessage.getSender());
			stmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
	        if (stmt != null) { 
	        	try {
	        		stmt.close();
	        	} catch (SQLException e) {} 
	        }
	    }
	}
    public void onMessage(String channel, String sender, String login, String hostname, String message) {
    	if(message.startsWith("!") || sender.equals("twitchnotify") || sender.equals("nightbot")) {
        	//ignore since its most likely a command
    		return;
        }
    	//create message object
    	ChatMessage messageObj = new ChatMessage(channel, sender, login, hostname, message);

    	//add users / channels if missing
    	if(!userExists(messageObj)) {
			insertNewUser(messageObj);
		} 
		if(!channelExists(messageObj)) {
			insertNewChannel(messageObj);
		}
        
        
        try {
			mainStatement.setString(1, messageObj.getSender());
			mainStatement.setString(2, messageObj.getChannel());
			mainStatement.setCharacterStream(3, new StringReader(messageObj.getMessage()), messageObj.getMessage().length());
			mainStatement.setTimestamp(4, new Timestamp(new DateTime().minusMillis(TimeZoneOffSetMillis).toDate().getTime()));
			mainStatement.addBatch();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	//queue.add(messageObj);
    }
    private class Consumer implements Runnable {

		@Override
		public void run() {
			while(!Thread.currentThread().isInterrupted()) {
				if(queue.size() > 0) {
					
					ChatMessage removedMessage = queue.remove();
					
					if(!userExists(removedMessage)) {
						insertNewUser(removedMessage);
					} 
					if(!channelExists(removedMessage)) {
						insertNewChannel(removedMessage);
					}
					//user & channel must exist
					insertNewMessage(removedMessage);
						
					
				}
			}
		}
		private void insertNewMessage(ChatMessage removedMessage) {
			PreparedStatement stmt = null;
			try {
				stmt = conn.prepareStatement("INSERT INTO Messages (username, channel, message, created_at) VALUES (?, ?, ?, ?)");
				stmt.setString(1, removedMessage.getSender());
				stmt.setString(2, removedMessage.getChannel());
				stmt.setCharacterStream(3, new StringReader(removedMessage.getMessage()), removedMessage.getMessage().length());
				stmt.setTimestamp(4, new Timestamp(new DateTime().minusMillis(TimeZoneOffSetMillis).toDate().getTime()));
				stmt.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
		        if (stmt != null) { 
		        	try {
		        		stmt.close();
		        	} catch (SQLException e) {} 
		        }
		    }
			//System.out.println("Inserted Message "+new Date().toString()+"From "+removedMessage.getSender()+" in "+removedMessage.getChannel()+": "+removedMessage.getMessage()+"");
		}
		private void insertNewUser(ChatMessage removedMessage) {
			PreparedStatement stmt = null;
			try {
				stmt = conn.prepareStatement("INSERT INTO TwitchUsers (username) VALUES (?)");
				stmt.setString(1, removedMessage.getSender());
				stmt.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
		        if (stmt != null) { 
		        	try {
		        		stmt.close();
		        	} catch (SQLException e) {} 
		        }
		    }
		}
		private boolean userExists(ChatMessage removedMessage) {
			Statement stmt = null;
			try {
				stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT COUNT(*) AS found FROM TwitchUsers WHERE username='"+removedMessage.getSender()+"';");
				while (rs.next()) {
					if(rs.getInt("found") != 0) {
						return true;
					}
				}
			} catch (SQLException e) {
				return false;
			} finally {
		        if (stmt != null) { 
		        	try {
		        		stmt.close();
		        	} catch (SQLException e) {} 
		        }
		    }
			return false;
		}
		private boolean channelExists(ChatMessage removedMessage) {
			Statement stmt = null;
			try {
				stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT COUNT(*) AS found FROM TwitchUsers WHERE username='"+removedMessage.getChannel()+"';");
				while (rs.next()) {
					if(rs.getInt("found") != 0) {
						return true;
					}
				}
			} catch (SQLException e) {
				return false;
			} finally {
		        if (stmt != null) { 
		        	try {
		        		stmt.close();
		        	} catch (SQLException e) {} 
		        }
		    }
			return false;
		}
    	
    }
    private void insertNewChannel(ChatMessage removedMessage) {
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement("INSERT INTO TwitchUsers (username) VALUES (?)");
			stmt.setString(1, removedMessage.getChannel());
			stmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
	        if (stmt != null) { 
	        	try {
	        		stmt.close();
	        	} catch (SQLException e) {} 
	        }
	    }
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
}