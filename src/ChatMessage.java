
public class ChatMessage {
	//String channel, String sender, String login, String hostname, String message
	private String message;
	private String sender;
	private String login;
	private String hostname;
	private String channel;
	
	ChatMessage(String channel, String sender, String login, String hostname, String message) {
		this.channel = channel.substring(1);
		this.sender = sender;
		this.login = login;
		this.hostname = hostname;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	
	public String getSender() {
		return sender;
	}

	

	public String getLogin() {
		return login;
	}

	

	public String getHostname() {
		return hostname;
	}

	

	public String getChannel() {
		return channel;
	}

	
}
