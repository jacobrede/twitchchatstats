import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;

import org.bson.Document;

import com.google.gson.Gson;
import com.mongodb.client.MongoDatabase;
import com.mysql.jdbc.Connection;

public class Stats  implements Comparable {
	String username;
	Timestamp stream_start_time;
	
	Date start_time;
	String dateStartFormat;
	Date end_time;
	String dateEndFormat;
	
	private int numberMessages;
	private HashMap<String, Integer> wordOccurrance;
	private ArrayList<SingleWordStat> sortedWordOccurrance;
	
	private List<String> stopWords = Arrays.asList("i","a","about","an","are","as","at","be","by","com","for","from","how","in","is",
	                                    "it","of","on","or","that","the","this","to","was","what","when","where","who","will","with","the","www", 
	                                    "me", "you", "my", "hi", "it's", "its", "and", "we", "he");
	
	Connection conn = null;
	
	/*
	 * should only be created with start & end time being 1 min apart
	 */
	Stats(String username, Date start_time, Date end_time, Timestamp stream_start_time, String DB_URL, String DB_USER, String DB_PASS) {
		this.wordOccurrance = new HashMap<String, Integer>();
		this.sortedWordOccurrance = new ArrayList<SingleWordStat>();
		this.numberMessages = 0;
		this.username = username;
		this.start_time = new Date(start_time.getTime());
		this.stream_start_time = new Timestamp(stream_start_time.getTime());
		this.dateStartFormat = (start_time.getYear()+1900)+"-"+(start_time.getMonth()+1)+"-"+start_time.getDate()+" "+start_time.getHours()+":"+start_time.getMinutes()+":"+start_time.getSeconds();
		this.end_time = new Date(end_time.getTime());
		this.dateEndFormat = (end_time.getYear()+1900)+"-"+(end_time.getMonth()+1)+"-"+end_time.getDate()+" "+end_time.getHours()+":"+end_time.getMinutes()+":"+end_time.getSeconds();

		try {
            conn = (Connection) DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
            // Do something with the Connection
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
		
		
		analyzeWords();
		sortResults();
		
		//close db connection
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void analyzeWords() {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			Timestamp SQLFormatStartDate = new Timestamp(start_time.getTime());
			String timeformart = SQLFormatStartDate.toString().substring(0, SQLFormatStartDate.toString().length()-4)+"%";
			//System.err.println("SELECT message, created_at FROM Messages WHERE channel='"+username+"' AND created_at LIKE '"+timeformart+"';");

			ResultSet rs = stmt.executeQuery("SELECT message, created_at FROM Messages WHERE channel='"+username+"' AND created_at LIKE '"+timeformart+"';");
			while (rs.next()) {
				//System.err.println(rs.getString("message"));
				String[] words = rs.getString("message").split("\\s+");
				for(String singleWord : words) {
					//make each word lowercase, to better capture duplicates
					singleWord = singleWord.toLowerCase();
					singleWord = getMatchingWord(singleWord);
					if(!stopWords.contains(singleWord)){
						if (wordOccurrance.containsKey(singleWord)) {
							int wordCount = wordOccurrance.get(singleWord);
							wordOccurrance.put(singleWord, ++wordCount);
						} else {
							//word doesnt exist, add it
							wordOccurrance.put(singleWord, 1);
						}
					}
				}
				numberMessages++;
			}
		} catch (SQLException e) {
			
		} finally {
	        if (stmt != null) { 
	        	try {
	        		stmt.close();
	        	} catch (SQLException e) {} 
	        }
	    }
	}
	private String getMatchingWord(String singleWord) {
		if(singleWord.matches("(?:a*(?:ha)+h?)")) {
			return "haha";
		} else if(singleWord.matches("(?:l+o+)+l+")) {
			return "lol";
		}
		return singleWord;
	}
	private void sortResults() {
		for(String singleWord : wordOccurrance.keySet()) {
			SingleWordStat newWordStat = new SingleWordStat(singleWord, wordOccurrance.get(singleWord));
			sortedWordOccurrance.add(newWordStat);
		}
		//sort, desc
		Collections.sort(sortedWordOccurrance ,Collections.reverseOrder());
		//only keep top 100 words, remove the rest
		if(sortedWordOccurrance.size() > 100) {
			//need to pop off values
			for(int j = sortedWordOccurrance.size()-1; j > 100; j--) {
				sortedWordOccurrance.remove(j);
			}
		}
	}
	public String getSQL() {
		return "INSERT INTO StreamMessagesStats (username, start_time, end_time, messagesPerMin, TopWords, stream_start_time) VALUES ("
				+ "'"+username+"', "
				+ "'"+new Timestamp(start_time.getTime())+"', "
				+ "'"+new Timestamp(end_time.getTime())+"', "
				+ ""+numberMessages+", "
				+ "'"+new Gson().toJson(sortedWordOccurrance)+"',"
				+ "'"+this.stream_start_time+"')";
	}
	public String getUsername() {
		return username;
	}
	public Date getStart_time() {
		return start_time;
	}
	public Date getStreamStart_time() {
		return stream_start_time;
	}
	public Date getEnd_time() {
		return end_time;
	}
	public int getNumberMessages() {
		return numberMessages;
	}
	public String getSortedWordOccurrance() {
		return new Gson().toJson(sortedWordOccurrance);
	}
	public ArrayList<SingleWordStat> getSortedWordOccurranceList() {
		return sortedWordOccurrance;
	}
	public Document getMongoDocumentToInsert() {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
		return new Document("username", username)
				.append("start_time", format.format(start_time))
				.append("end_time", format.format(end_time))
				.append("stream_start_time", format.format(stream_start_time))
				.append("messagesPerMin", numberMessages)
				.append("TopWords", new Gson().toJson(sortedWordOccurrance));
	}
	public HashMap<String, Integer> getWordOccurranceMap() {
		return wordOccurrance;
	}
	public int compareTo(Object o) {
		if(o instanceof Stats) {
			Stats copyOfStats = (Stats) o;
			if(this.getNumberMessages() > copyOfStats.getNumberMessages()) {
				return 1;
			} else if(this.getNumberMessages() < copyOfStats.getNumberMessages()){
				return -1;
			} else {
				return 0;
			}
		} else {
			return -1;
		}
	}
}
